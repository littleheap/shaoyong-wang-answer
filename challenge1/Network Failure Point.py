import collections


def identify_router(graph):
    d = collections.defaultdict(int)
    for ele in graph:
        d[ele[0]] += 1
        d[ele[1]] += 1
    res = []
    high = max(d.items(), key=lambda x: x[1])[1]
    for key, conn in d.items():
        if conn == high:
            res.append(key)
    return res


test1 = [[1, 2], [2, 3], [3, 5], [5, 2], [2, 1]]
test2 = [[1, 3], [3, 5], [5, 6], [6, 4], [4, 5], [5, 2], [2, 6]]
test3 = [[2, 4], [4, 6], [6, 2], [2, 5], [5, 6]]

print(identify_router(test1))
# [2]
print(identify_router(test2))
# [5]
print(identify_router(test3))
# [2,6]

# Time complexity : O(n) - Because I use the dictionary and go through once.