from challenge2 import sqlfunction as sql
from flask import Flask,request, render_template


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/signup', methods=['POST'])
def signup():
    return render_template('signup.html')


@app.route('/signup_confirm', methods=['POST'])
def signup_confirm():
    keys = ['ID', 'Password']
    values = []
    for key in keys:
        values.append(request.form.get(key, None))
    if sql.signup(values):
        return "Success"
    else:
        return "Fail"


if __name__ == '__main__':
    app.debug = True
    app.run()
