Sorry I have little experience of Django, so I write a simple
case using Flask and I know the target of the test.

I will create a procedure in the mysql and check the time 
whether or not the 'sysdate - start_date > 7' and set the 
request attribute of user from 0 to 1.

And then create a python script or node.js script to check 
the request attribute of all users. If it is '1', I will use 
the Stripe API to request money for the user and set the request 
attribute as '0'. 