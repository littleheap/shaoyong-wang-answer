import pymysql

database_name = 'test'

conn = pymysql.connect(host='localhost',
                       port=3306,
                       user='root',
                       password='12345',
                       db=database_name,
                       charset='utf8')
cursor = conn.cursor()


def signup(values):
    try:
        id, password = values
        cursor.execute(
            "insert into User (id, password, start_date, subscribe, request) values (%s, %s, sysdate, 1, 0);", id,
            password)
        conn.commit()
        return True
    except:
        return False


def signin(values):
    try:
        id, password = values
        cursor.execute("select * from User where id = %s;", id)
        results = cursor.fetchall()
        pwd = results[0][1]
        if pwd == password:
            return True
        else:
            return False
    except:
        return False


def close_database():
    cursor.close()
    conn.close()
